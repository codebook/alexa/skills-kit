## Can the initial interaction with Alexa do nothing more than launch the skill?

Hi.

I have submitted my Alexa Skill, Bus Visor, twice and have been rejected both times. I hope you can help me with a couple of questions:

1. Can the initial turn of the dialog with Alexa do nothing more than launch the app?

2. Once launched, can Alexa take charge and prompt for two bits of information in turn before ultimately carrying out the user's request?

I ask because my skill returns real-time arrival information of the buses of the public transit system of the City of New York. As submitted, the the flow is this:

a. User says "Alexa, Open Bus Visor" b. Alexa says something like "Which bus route will you be riding". c. User says "I will be riding the M20". d. Alexa says something like "What stop?" e. Users says "Bedford street". f. Alexa says something like "There is a bus 1 mile away, ETA 5 minutes"

To that end when I submitted my app I used the three user utterances

Open Bus Visor I will be riding the M20 Bedford Street

as sample phrases in the publishing information section of the developer portal. This works for me on my device and on the simulator. The rejection note complains about the 3rd utterance. It is not clear from the note if that utterance is used alone or after the first two. If alone, as I suspect, it is not expected to work. That is the three phrases comprise a single sample.

Please note that I am fine if you tell me that your device is not to be used in a conversational mode like mine I am fine with withdrawing it. I had hoped to develop the Alexa and Google Assistant versions in parallel but if you don't then you don't and that's fine.

But if you allow a mode where the user has to answer TWO questions after launch before satisfaction then I would appreciate it if you could tell me how I indicate that in the portal.

Thanks in advance.

Regards, Will



## My Answer

As a general comment, ASK (although it's a very small SDK) is not the easiest SDK to understand and to program with. It took me a while to get the hang of it. There is an underlying Alexa Skills platform (which is essentially a web services), and ASK resides on top of this. I will presume you are using ASK for Node.js.

ASK is essentially based on a question-answer paradigm. Alexa (or, Alexa Skill) asks and the user answers. If Alexa "tells", and not "asks", then the session ends. The user can ask a question back in response to Alexa's question, but it is much easier, conceptually, to view even that as an answer. (Even Alexa's 'ask' doesn't have to be a real question.) So, here's a set of possible flows in a "session" in an Alexa Skill:

Alexa Tells
Alexa Asks - User Answers (or, asks a question or whatever) - Alexa Tells
Alexa Asks - User Answers - Alexa Asks - User Answers - Alexa Tells
Alexa Q - User A - Alexa Q - User A - Alexa Q - User A - Alexa T.
....
(I'm really new to ASK but I find this model much easier to visualize than viewing it in a more general way. Remember, Alexa/Skill asks and the user answers.)

@Will, to answer your first question, you can launch your skill in two ways. (A) Just launch and (B) launch and start an intent.

For (A), Alexa starts your skill, and Alexa asks a question on your skill's behalf (b.). You do this by writing a handler for built-in intent "LaunchRequest". At this point, the user has to "answer", which points to a particular intent in your skill. In your example, "I will be riding the M20". Let's suppose this utterance is mapped to RideRouteIntent. In your code, you will need to handle this intent and your skill has to "ask" another question, in response, to keep the session alive. ("What stop?" in your example). Then, the user answers ("Bedford street"). Let's suppose that this answer is mapped to RideStopIntent. Finally, Alexa (or your skill) "tells" the answer to the user's answer (which is really a question semantically), like "There is a bus 1 mile away, ETA 5 minutes". You do this by writing a handler to the RideStopIntent. I hope this all makes sense to you.

There is nothing wrong in doing "conversational mode" in Alexa. You just have to keep track of what your skill and the user are doing. I hope the answer to your question 2. is obvious once you understand this flow.

For your example, you need two custom Intents and handlers for at least three intents, the LaunchRequest and, say, RideRouteIntent and RideStopIntent. The utterances like "I will be riding {routeName}" should be mapped to RideRouteIntent and the utterances like "I'm at bus stop, {stopName}." should be mapped to RideStopIntent.

As you can see, if you follow my logic, you will need three SEPARATE utterances. ("Open Bus Visor" is really a default launch sentence handled by Alexa herself, and you need to handle two different types of utterances, mapped to the two aforementioned intents, respectively.)

This is how *I* would create a skill like yours. I hope this makes sense to you. If you want to use a launch with intent path (B), you simply handle the appended intent (with launch) in your custom intent handlers.

For what you are trying to do, saying three utterances in one sentence (without question-answer conversation), things are, in some sense, easier because you can handle everything in one intent. But, based on other parts your question, it doesn't seem like that's really what you want. (Your a. b. c. .. flow clearly indicates you want the dialog not saying the three utterances in one sentence.)

If you really want to do that (in addition to/instead of the conversational flow), you can easily do that as well (again, (B)). The user will say something like "Ask Bus Visor to tell me when the M20 will arrive at stop Bedford street". And, it is a SINGLE utterance (not three). Utterances like this (with two custom type slots, one for the bus route and the other for the bus stop) should be mapped to an intent like BusRouteStopArrivalTimeIntent. Now, your skill can support (1) one intent, BusRouteStopArrivalTimeIntent, (2) two intents, RideRouteIntent and RideStopIntent, or better yet (3) all three intents. (Or, some variations like providing bus route while launching but asking the user for the bus stop later, etc.) I suspect that your code is more like (1) while your intention is (2) or (3).

When you submit your skill for certification, you will need to be clear, and consistent, as to what your skill supports.

I hope this helps you develop a great Alexa skill.





